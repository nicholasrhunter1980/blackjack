require("dotenv").config();
const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const api = require("./api");
const port = 3030;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");
const Gambler = require("./models/Gambler");

if (process.env.MODE == "production") {
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: false,
      store: MongoStore.create({
        mongoUrl: `mongodb://2217030:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/2217030?authSource=admin`,
      }),
    })
  );
} else {
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: false,
      store: MongoStore.create({ mongoUrl: "mongodb://localhost/goose" }),
    })
  );
}
app.use(passport.initialize());
app.set("view engine", "ejs");
app.use(
  morgan("dev"),
  bodyParser.json({ extended: false }),
  bodyParser.urlencoded({ extended: false })
);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect("/");
}

const serveTablePage = (req, res) => {
  res.sendFile(path.join(__dirname, "./public/table.html"));
};
app.get("/table", ensureAuthenticated, serveTablePage);
app.get("/table.html", ensureAuthenticated, serveTablePage);
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/index.html"));
});

app.get("/profile", ensureAuthenticated, async (req, res) => {
  console.log("user", req.user);
  const gambler = await Gambler.findById(req.user.id);
  res.render("profile", { gambler });
});

app.use(express.static("public"), express.static("dist"));

app.use("/api", api);
async function main() {
  if (process.env.MODE == "production") {
    await mongoose.connect(`mongodb://192.168.171.67:27017/2217030`, {
      useNewUrlParser: true,
      authSource: "admin",
      user: "2217030",
      pass: process.env.MONGO_PASSWORD,
    });
  } else {
    await mongoose.connect("mongodb://localhost:27017/goose");
  }

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
  });
}
main().catch((err) => console.error(err));
